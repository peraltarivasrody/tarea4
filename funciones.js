const form = document.getElementById('form-calculadora');
      const resultado = document.getElementById('resultado');

      form.addEventListener('submit', (event) => {
        event.preventDefault();
        const num1 = parseFloat(form.numero1.value);
        const num2 = parseFloat(form.numero2.value);

        for (let i = 1; i <= 5; i++) {
          switch (i) {
            case 1:
              resultado.innerHTML += `Suma: ${num1 + num2}<br>`;
              break;
            case 2:
              resultado.innerHTML += `Resta: ${num1 - num2}<br>`;
              break;
            case 3:
              resultado.innerHTML += `Multiplicación: ${num1 * num2}<br>`;
              break;
            case 4:
              resultado.innerHTML += `División: ${num1 / num2}<br>`;
              break;
            case 5:
              resultado.innerHTML += `Módulo: ${num1 % num2}<br>`;
              break;
            default:
              break;
          }
        }
      });
      